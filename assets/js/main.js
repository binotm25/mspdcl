"use strict";

var pathArray = window.location.pathname.split( '/' );
var urlPath = location.protocol+'//'+location.hostname+'/'+pathArray[1]+'/'+pathArray[2];
var url = location.protocol+'//'+location.hostname+''+location.pathname; 
var checkPath = pathArray[pathArray.length - 1];
var secondCheck = pathArray[pathArray.length - 2];
var thirdCheck = pathArray[pathArray.length - 3];
var fourthCheck = pathArray[pathArray.length - 4];
var baseUrl = window.location.protocol + "//" + window.location.host;
var currentUrl = window.location.href;
window.costEstimateCurrentDiscount = 0;

var SDLC = window.SDLC || {};

SDLC.ContactUs = new function()
{
    var self = this;

    this.submitForm = function()
    {
        $("#submit-form").on("click", function(){
            
            $("#contact-us-form").parsley().validate();

            self.check = $('.parsley-error').length === 0;

            if(self.check){
                swal({
                    title: 'Are you sure?',
                    text: "Please check all the required inputs.",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, submit it!'
                }).then((result) => {
                    if (result) {
                        swal(
                            'Done!',
                            'Your queries has been submitted.',
                            'success'
                        ).then(() => {
                            $("#contact-us-form .input1").removeClass('parsley-success');
                            $("#contact-us-form")[0].reset();
                        });
                    }
                });
            }
            

        });
    }
}

SDLC.Gallery = new function()
{
    var self = this;

    this.imagePop = function()
    {
        
    }
}

console.log(checkPath);

if(checkPath == "contact.html"){

    let contactController = SDLC.ContactUs;
    contactController.submitForm();

}else if(checkPath == "gallery.html"){
    $(document).ready(function ($) {
        // delegate calls to data-toggle="lightbox"
        $(document).on('click', '[data-toggle="lightbox"]:not([data-gallery="navigateTo"])', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function() {
                    if (window.console) {
                        return console.log('Checking our the events huh?');
                    }
                },
                onNavigate: function(direction, itemIndex) {
                    if (window.console) {
                        return console.log('Navigating '+direction+'. Current item: '+itemIndex);
                    }
                }
            });
        });
        //Programmatically call
        $('#open-image').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });
        $('#open-youtube').click(function (e) {
            e.preventDefault();
            $(this).ekkoLightbox();
        });
        // navigateTo
        $(document).on('click', '[data-toggle="lightbox"][data-gallery="navigateTo"]', function(event) {
            event.preventDefault();
            return $(this).ekkoLightbox({
                onShown: function() {
                    this.modal().on('click', '.modal-footer a', function(e) {
                        e.preventDefault();
                        this.navigateTo(2);
                    }.bind(this));
                }
            });
        });
    });
}